const p = require('perf_hooks').performance;

var numbers = [];

main = (input) => {
    var startTime = p.now()
    console.log("-----------------------------------------------");
    parseInput(input);

    console.log("Day 9A: " + findFaultyNumberA() + " in " + (p.now() - startTime) + " ms");
    console.log("Day 9B: " + findContiguousNumberB() + " in " + (p.now() - startTime) + " ms");
}


findFaultyNumberA = () => {
    for (let i = 25; i < numbers.length; i++) {
        if (!checkNumbersA(numbers.slice(i - 25, i), numbers[i])) {
            return numbers[i]
        };
    }
}

checkNumbersA = (array, sum) => {
    var found = false;
    for (let i = 0; i < array.length; i++) {
        value = array[i];
        found = ((sum - value != value) && array.includes(sum - value));
        if (found) {
            break;
        }
    }
    return found;
}

findContiguousNumberB = () => {
    var value = findFaultyNumberA();
    for (let i = 25; i < numbers.length; i++) {
        var result = (checkNumbersB(numbers.slice(i - 25, i), value));
        if (result.found) {
            return result.sum;
        }
    }
}

// The easy way
checkNumbersB = (array, amount) => {
    var sum = 0;
    var index = 0;
    var range = [];

    while(index < array.length){
        let value = array[index];
        range.push(value);
        sum += value;        // range.reduce() is too slow, so we add up separately

        if (sum === amount){
           return { found: true, sum: getMax(range) + getMin(range) }
        }

        if (sum > amount){            
            sum -= range[0];
            range.shift();
            if (sum === amount){
                return { found: true, sum: getMax(range) + getMin(range) }
            }
        }
        index++;
    }
    return { found: false, sum: 0 }
}

// The hard way
checkNumbersB2 = (array, amount) => {
    var index = 0;
    var range = [];
    var result = { found: false, sum: 0 }

    // Aggregate is not equal to the sum but we haven't checked all possibilities yet
    // array-length - 1 since it's no point checking only the last number, we need at least two
    while ((aggregate !== amount) && (index < array.length - 1)) {       
        range = [];
        var innerIndex = index;
        var aggregate = 0;

        // As long as we haven't reached the required amount yet and there's still numbers left to add
        // keep adding the next number. After every outer loop, the starting point of the inner loop moves one index further.
        // this makes sure that if array[1] + array[2] + array[3] > amount, it will then try with just array[2] + array[3].
        while ((aggregate < amount) && (innerIndex < array.length)) {
            aggregate += array[innerIndex];
            
            // Keep track of the numbers so we can later refer to it
            range.push(array[innerIndex]);
            innerIndex++
        }
        index++;

        // We ran through the whole array and are still below the required amount, further loops are pointless
        if (aggregate < amount) {
            break;
        }
    }

    if (aggregate === amount) {
        result = { found: true, sum: getMax(range) + getMin(range) };
    }
    return result;
}

getMax = (array) => {
    return array.reduce((a, b) => { return Math.max(a, b)})
}

getMin = (array) => {
    return array.reduce((a, b) => { return Math.min(a, b)})
}

parseInput = (raw) => {
    numbers = raw.toString()
        .replace(/\r/g, "")
        .split(/\n/)
        .map(value => parseInt(value))
}

module.exports = {
    run: main
}
