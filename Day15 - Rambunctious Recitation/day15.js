const p = require('perf_hooks').performance;

var numbers = [0, 5, 4, 1, 10, 14, 7];
var numbersSmall = [3,1,2];
var indexToCheck = 2020;

var numbersSpoken = [];
var index = 0;
var object = {};

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    console.log("Day 15A: " + calculate(numbers) + " in " + (p.now() - startTime) + " ms");
    numbersSpoken = [];
    index = 0;
    indexToCheck = 30000000;
    //console.log("Day 15B: " + calculate(numbers) + " in " + (p.now() - startTime) + " ms");

}

calculate = (array) => {
    var lastNumber;
    var firstRun = true;
    while (index < indexToCheck){
        
        if (firstRun){
            for (let i=0; i<array.length; i++){
                speakNumber(array[i]);
                lastNumber = array[i];
                index++;
            }
            firstRun = false;
        }
       
        var object = numbersSpoken[lastNumber];
        if (object.diff === 0){
            speakNumber(0);
            lastNumber = 0;
        } else {
            speakNumber(object.diff);
            lastNumber = object.diff;
        }
        index++;
    }
    return lastNumber;
}

speakNumber = (number) => {
    object = numbersSpoken[number];
    if (object !== undefined) {
        numbersSpoken[number] = { idx: index, diff: (index - object.idx)};
    } else {
        numbersSpoken[number] = { idx: index, diff: 0 };
    }
}

module.exports = {
    run: main
}
