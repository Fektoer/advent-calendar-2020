const p = require('perf_hooks').performance;

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    console.log("Day 6A: " + countAnswersA(input) + " in " + (p.now() - startTime) + " ms");
    console.log("Day 6B: " + countAnswersB(input) + " in " + (p.now() - startTime) + " ms");
}

countAnswersA = (raw) => {
    var numAnswers = 0;
    raw.toString()
        .replace(/\r/g, "")                                                     //remove weird symbols
        .split(/\n/)                                                            //split per group
        .map(answer => answer === '' ? '\n' : answer)                           //replace the empty character with a new line symbol
        .join('')                                                               //concat everything
        .split(/\n/)                                                            //split at the added new line so you have a string per group
        .map(answer => answer.split('')                                         //Per groupstring, split the characters into an array 
            .filter(function (item, i, ar) { return ar.indexOf(item) === i; })  //Filter unique values
            .join(''))                                                          //Join so you have a groupstring with solely unique values
        .forEach(element => numAnswers += element.length);                      //For each group, count the characters and add to total
    return numAnswers;
}

countAnswersB = (raw) => {
    var numAnswers = 0;
    groupArray = raw.toString()
        .replace(/\r/g, "")                                                     //remove weird symbols
        .split(/\n/)                                                            //split per group
        .map(answer => answer === '' ? '\n' : answer)                           //replace the empty character with a new line symbol
        .join(';')                                                              //concat everything with a delimiter for later
        .split(/\n/)                                                            //split at the added new line so you have a string per group, split by a delimiter
        .map(answer => answer.split(';')                                        //spit the groupstring at the delimiter
            .filter(item => item !== ''))                                       //without any empty values

    for (i = 0; i < groupArray.length - 1; i++) {                               //last line is empty
        var group = groupArray[i];
        var firstSetOfAnswers = group[0].split('');                             //take the answers of the first person
        var sharedAnswers = 0;
        firstSetOfAnswers.forEach(item => {                                     //for each answer of that person
            var bool = true;
            for (let j = 1; j < group.length; j++) {                            //get the answers of the other person
                var answer = item;
                bool = bool && (group[j].split('')                              //only true if all other persons
                    .filter(item => item === answer)                            //have that answer too
                    .length > 0)
            }
            if (bool) {
                sharedAnswers++
            }
        })
        numAnswers += sharedAnswers;
    }
    return numAnswers;
}

module.exports = {
    run: main
}
