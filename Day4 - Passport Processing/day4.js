const p = require('perf_hooks').performance;

main1 = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    console.log('Day 4A: ' + countValidPassportsA(parseInput(input)) + " in " + (p.now() - startTime) + " ms");
    console.log('Day 4B: ' + countValidPassportsB(parseInput(input)) + " in " + (p.now() - startTime) + " ms");
}

parseInput = (raw) => {
    var passportMaps = [];
    let tableOfPassports = raw.toString().replace(/\r/g,"").split("\n\n")

    for (i = 0; i < tableOfPassports.length; i++) {
        let passportRecord = tableOfPassports[i].split(/\n| /);
        let passportMap = new Map();
        for (j = 0; j < passportRecord.length; j++) {
            let fields = passportRecord[j].split(":");
            passportMap.set(fields[0], fields[1]);
        }
        passportMaps.push(passportMap);
    }
    return passportMaps;
}

countValidPassportsA = (passports) => {
    var numValidPassports = 0;
    for (i = 0; i < passports.length; i++) {
        let passportMap = passports[i];
        if (passportMap.has('byr') &&
            passportMap.has('iyr') && 
            passportMap.has('eyr') && 
            passportMap.has('hgt') && 
            passportMap.has('hcl') && 
            passportMap.has('ecl') && 
            passportMap.has('pid')) {
            numValidPassports++;
        }
    }
    return numValidPassports;
}

countValidPassportsB = (passports) => {
    var numValidPassports = 0;
    for (i = 0; i < passports.length; i++) {
        let passportMap = passports[i];
        if ((passportMap.has('byr') && birthyearIsValid(passportMap.get('byr'))) &&
            passportMap.has('iyr') && issueYearIsValid(passportMap.get('iyr')) &&
            passportMap.has('eyr') && expirationYearIsValid(passportMap.get('eyr')) &&
            passportMap.has('hgt') && heightIsValid(passportMap.get('hgt')) &&
            passportMap.has('hcl') && hairColorIsValid(passportMap.get('hcl')) &&
            passportMap.has('ecl') && eyeColorIsValid(passportMap.get('ecl')) &&
            passportMap.has('pid') && passPortIdIsValid(passportMap.get('pid'))) {
            numValidPassports++;
        }
    }
    return numValidPassports;
}

birthyearIsValid = (input) => {
    let byr = parseInt(input); 
    return input.length = 4 && (byr >= 1920 && byr <= 2002);
}

issueYearIsValid = (input) => {
    let iyr = parseInt(input); 
    return input.length = 4 && (iyr >= 2010 && iyr <= 2020);
}

expirationYearIsValid = (input) => {
    let eyr = parseInt(input); 
    return input.length = 4 && (eyr >= 2020 && eyr <= 2030);
}

heightIsValid = (input) => {
    let valid;
    let parsedHeight = input.match(/[^\d]+|\d+/g);
    let height = parseInt(parsedHeight[0]);
    if (parsedHeight[1] === 'cm'){    
        valid = height >= 150 && height <= 193;
    } else if (parsedHeight[1] === 'in'){    
        valid = height >= 59 && height <= 76;
    } else {
        valid = false;
    }
    return valid;
}

hairColorIsValid = (input) => {
    return input.match(/#[a-z0-9]{6}/);
}

eyeColorIsValid = (input) => {
    return (input === 'amb' ||
            input === 'blu' ||
            input === 'brn' ||
            input === 'gry' ||
            input === 'grn' ||
            input === 'hzl' ||
            input === 'oth')
}

passPortIdIsValid = (input) => {  
    return input.match(/^[0-9]{9}$/);
}

module.exports = {
    run: main1
}
