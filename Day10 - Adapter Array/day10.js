const p = require('perf_hooks').performance;

var numbers = [];
var routeDictionary = [];

main = (input) => {
    var startTime = p.now()
    console.log("-----------------------------------------------");
    parseInput(input);

    console.log("Day 10A: " + calculateA() + " in " + (p.now() - startTime) + " ms");
    console.log("Day 10B: " + calculateB() + " in " + (p.now() - startTime) + " ms");
}

parseInput = (raw) => {
    numbers = raw.toString()
        .replace(/\r/g, "")
        .split(/\n/)
        .map(value => parseInt(value))
        .sort((a, b) => a - b);
}

calculateB = () => {

    // Construct full route
    var finalAdapter = numbers[numbers.length - 1] + 3;
    numbers.push(finalAdapter);                             // end
    numbers.push(0);                                        // start
    numbers.sort((a, b) => a - b);

    // Create graph
    for (let i = 0; i < numbers.length; i++) {
        let vertices = [];
        let value = numbers[i];
        if (numbers.includes(value + 1)) { vertices.push(value + 1); }
        if (numbers.includes(value + 2)) { vertices.push(value + 2); }
        if (numbers.includes(value + 3)) { vertices.push(value + 3); }
        routeDictionary[value] = vertices;
    }

    // Calculate all paths from start to end with an empty cache
    return calculatePath(0, finalAdapter, []);
}

calculatePath = (value, target, localcache) => {

    // Route already traversed, return memorized routes
    if (localcache[value]){ 
        return localcache[value] }
    else {

        // Destination reached
        if (value === target) {
            return 1;
        } else {
            let result = 0;
            var vertices = routeDictionary[value];
            for (let i = 0; i < vertices.length; i++) {

                // Calculate routes from child
                result += calculatePath(vertices[i], target, localcache);

                // Memoization by caching the result
                localcache[value] = result;
            }
            return result;
        }
    }
}

calculateA = () => {
    var jolt1 = 0;
    var jolt3 = 0;

    for (let i = 1; i < numbers.length; i++) {
        let diff = numbers[i] - numbers[i - 1];

        switch (diff) {
            case 1: jolt1++; break;
            case 3: jolt3++; break;
        }
    }
    return (jolt1 + 1) * (jolt3 + 1);
}

module.exports = {
    run: main
}
