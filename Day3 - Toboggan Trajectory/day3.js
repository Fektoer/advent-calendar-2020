const p = require('perf_hooks').performance;

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    table = input.toString().replace(/\r/g,"").split("\n");
    console.log('Day 3A: ' + goDownSlopeAndCountTrees(table, 3, 1) + " in " + (p.now() - startTime) + " ms");
    console.log('Day 3B: ' + countAllSlopes(table) + " in " + (p.now() - startTime) + " ms");
}

countAllSlopes = (slope) => {
    return goDownSlopeAndCountTrees(slope, 1, 1) *  //64
        goDownSlopeAndCountTrees(slope, 3, 1) *  //284
        goDownSlopeAndCountTrees(slope, 5, 1) *  //71
        goDownSlopeAndCountTrees(slope, 7, 1) *  //68
        goDownSlopeAndCountTrees(slope, 1, 2)    //40  
}

goDownSlopeAndCountTrees = (slope, right, down) => {
    const tree = '#';
    let totalTrees = 0;
    let horizontalIndex = 0;
    let rowLength = slope[0].length;

    for (i = 0; i < slope.length; i = i + down) {
        let nextRow = slope[i + down];
        try {
            if (nextRow == '' || nextRow == undefined) {
                break;
            }

            horizontalIndex = (horizontalIndex + right) % rowLength;
            charAtPosition = nextRow.charAt(horizontalIndex);
            if (charAtPosition === tree) {
                totalTrees++
            }
        } catch { }
    }
    return totalTrees;
}

module.exports = {
    run: main
}

