const p = require('perf_hooks').performance;

var seatPlan = [];
var hasChanged = true;
var recursionForB = false;
var limitOfAdjacentSeats = 3;

main = (input) => {
    var startTime = p.now()
    console.log("-----------------------------------------------");
    parseInput(input);
    console.log("Day 11A: " + calculateTotal() + " in " + (p.now() - startTime) + " ms");

    // Set param for B
    startTime = p.now()
    recursionForB = true;
    limitOfAdjacentSeats = 4;
    hasChanged = true;
    console.log("Day 11B: " + calculateTotal() + " in " + (p.now() - startTime) + " ms");
}

calculateTotal = () => {
    var newPlan = seatPlan;
    while (hasChanged) {
        hasChanged = false;
        newPlan = sitDownOrGetUp(newPlan);
    }

    // Count seats
    var count = 0;
    newPlan.map((x) => x.map((y) => { if (y === '#') { count++ } }));
    return count;
}

sitDownOrGetUp = (originalPlan) => {

    // Create a copy of the original plan. Mutations are made on this plan. Checks are done based on the original plan
    // That way you can 'commit' all changes simultaneously by assigning the new (mutated) plan to the old plan.
    var newPlan = originalPlan.map((x) => x.map((y) => y));
    for (let i = 0; i < originalPlan.length; i++) {
        for (let j = 0; j < originalPlan[i].length; j++) {
            if ((originalPlan[i][j] === 'L') &&
                (countAdjacentOccupiedSeats(i, j, originalPlan) === 0)) { //No adjacent seats taken
                newPlan[i][j] = '#';
                hasChanged = true;
            } else {
                if ((originalPlan[i][j] === '#') &&
                    (countAdjacentOccupiedSeats(i, j, originalPlan) > limitOfAdjacentSeats)) {  //Maximum of adjacent seats taken
                    newPlan[i][j] = 'L';
                    hasChanged = true;
                }
            }
        }
    }
    return newPlan;
}

countAdjacentOccupiedSeats = (i, j, plan) => {
    var count = 0
    
    //Count the occupied seats around you
    count += hasAdjacentOccupiedSeat(-1, -1, i, j, plan);  //Top left
    count += hasAdjacentOccupiedSeat(-1, 0, i, j, plan);   //Top mid
    count += hasAdjacentOccupiedSeat(-1, 1, i, j, plan);   //Top right
    count += hasAdjacentOccupiedSeat(0, 1, i, j, plan);    //Right
    count += hasAdjacentOccupiedSeat(1, 1, i, j, plan);    //Bottom right
    count += hasAdjacentOccupiedSeat(1, 0, i, j, plan);    //Bottom mid
    count += hasAdjacentOccupiedSeat(1, -1, i, j, plan);   //Bottom left
    count += hasAdjacentOccupiedSeat(0, -1, i, j, plan);   //Left
    return count;
}

hasAdjacentOccupiedSeat = (horizontalOffset, verticalOffset, i, j, plan) => {
    var offsetRow = plan[i + horizontalOffset];
    var offSetColumn = plan[j + verticalOffset];
    var char = '.';

    if ((offsetRow) && (offSetColumn)) {
        char = plan[i + horizontalOffset][j + verticalOffset];
        if (recursionForB) {
            if (char === '.') {

                // Search one seat further (meaning indices shift along that direction)
                if (horizontalOffset !== 0) {
                    horizontalOffset = (horizontalOffset > 0) ? horizontalOffset + 1 : horizontalOffset - 1;
                }
                if (verticalOffset !== 0) {
                    verticalOffset = (verticalOffset > 0) ? verticalOffset + 1 : verticalOffset - 1;
                }
                return hasAdjacentOccupiedSeat(horizontalOffset, verticalOffset, i, j, plan);
            } else {
                return (char === '#') ? 1 : 0;
            }
        } else {
            return (char === '#') ? 1 : 0;
        }
    } else {
        // Out of bounds, not an occupied seat by default
        return 0;
    }
}

parseInput = (raw) => {
    raw.toString()
        .replace(/\r/g, "")
        .split(/\n/)
        .map(value => seatPlan.push(value.split('')))
}

module.exports = {
    run: main
}
