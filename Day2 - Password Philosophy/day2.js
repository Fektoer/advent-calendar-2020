const p = require('perf_hooks').performance;

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    table = input.toString().replace(/\r/g,"").split("\n");
    console.log("Day 2A: " + countValidPassWordsA(table) + " in " + (p.now() - startTime) + " ms");
    console.log("Day 2B: " + countValidPassWordsB(table) + " in " + (p.now() - startTime) + " ms");
}

countValidPassWordsA = (table) => {
    let result = 0;

    for (i = 0; i < table.length - 1; i++) {
        let tableRecord = table[i].split(" ");
        let tableMinMax = tableRecord[0].split("-");

        let min = parseInt(tableMinMax[0]);
        let max = parseInt(tableMinMax[1]);
        let character = tableRecord[1].slice(0, -1);
        let password = tableRecord[2];

        if (password !== ''){
            try{
                let numOccurrence = password.match(new RegExp(character, "g")).length;
                if (numOccurrence >= min && numOccurrence <= max){
                    result++
                }
            }
            catch {}
        }       
    }
    return result;
}

countValidPassWordsB = (table) => {
    let result = 0;

    for (i = 0; i < table.length - 1; i++) {
        let tableRecord = table[i].split(" ");
        let tableMinMax = tableRecord[0].split("-");

        let pos1 = parseInt(tableMinMax[0]) - 1;
        let pos2 = parseInt(tableMinMax[1]) - 1;
        let character = tableRecord[1].slice(0, -1);
        let password = tableRecord[2];

        //XOR
        if (((password[pos1] === character) && !(password[pos2] === character)) || (!(password[pos1] === character) && (password[pos2] === character))) {
            result++;
        }
    }
    return result;
}

module.exports = {
    run: main
}
