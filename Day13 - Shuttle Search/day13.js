const p = require('perf_hooks').performance;

var busId = 0;
var busesFiltered = [];
var busesFull = [];


main = (input) => {
    var startTime = p.now()
    console.log("-----------------------------------------------");
    parseInput(input);
    console.log("Day 13A: " + calculateA() + " in " + (p.now() - startTime) + " ms");
    console.log("Day 13B: " + calculateB() + " in " + (p.now() - startTime) + " ms");
}

calculateA = () => {
    var departures = [];
    for (let i = 0; i < busesFiltered.length; i++) {
        let departureDifference = busesFiltered[i] - (busId % busesFiltered[i]);
        departures.push([busesFiltered[i], departureDifference]);
    }

    departures.sort((a, b) => a[1] - b[1]);
    return departures[0][0] * departures[0][1];
}

calculateB = () => {
    var departureTime = 0;
    var lcm = 1; // Lowest Common Multiple

    for (let i=0; i<busesFiltered.length; i++){
        var found = false;
        while (!found){
            var bus = busesFiltered[i];
            var timeDiff = busesFull.indexOf(bus);
            
            // Bus leaves every x min (x being the busId). Departuretime % bus is there 0.
            // To check if a bus leaves the minute after, you calculate (departureTime + 1) % bus === 0.
            if (((departureTime + timeDiff) % bus) === 0){

                // We found a time where bus1 leaves at t0 and bus2 leaves t+n, it means that the next time
                // this happens is bus1 * bus2. For example: the first time bus 7 and bus 13 leave at a time where 
                // t0 % bus1 = 0 AND (t+1) % bus2 = 0 is t77. 77 % 7 = 0 and (77+1) % 13 = 0. The next time this happens
                // is (7*13=) 91 minutes later since the input consists of prime numbers (LCM). So the trick when checking a third bus 
                // is to loop over all moments where bus1 and bus2 coincide and check bus3 for those moments. 
                // This means we start at 77 (first time it bus1 and bus2 coincided) and count in steps of 91 (since that's the next time this will repeat).
                // We check that time to be valid for bus3 ((departuretime + offset) % bus3 = 0). Once we found that bus we 
                // multiply the LCM with bus3 since the only time bus1, bus2 AND bus3 coincide is bus1 * bus2 * bus3. Rinse and repeat. 
                lcm*= bus;
                found = true;
            } else {

                // Didn't find a matching departure time. Add the LCM to the departure time to see if the next sequence is valid
                departureTime += lcm;
            }
        }
    }
    return departureTime;
}

parseInput = (raw) => {
    var input = raw.toString()
        .replace(/\r/g, "")
        .split(/\n/);

    busesFull = input[1].split(',')
        .map((value) => (value !== 'x') ? parseInt(value) : value);
    busesFiltered = busesFull.filter((value) => (value !== 'x'))
}

module.exports = {
    run: main
}