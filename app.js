const fs = require('fs')

// Day 1: Report Repair
const day1 = require('./Day1 - Report Repair/day1.js')
day1.run(fs.readFileSync('./Day1 - Report Repair/input.txt'));

// Day 2: Password Philosophy
const day2 = require('./Day2 - Password Philosophy/day2.js')
day2.run(fs.readFileSync('./Day2 - Password Philosophy/input.txt'));

// Day 3: Toboggan Trajectory
const day3 = require('./Day3 - Toboggan Trajectory/day3.js')
day3.run(fs.readFileSync('./Day3 - Toboggan Trajectory/input.txt'));

// Day 4: Passport Processing
const day4 = require('./Day4 - Passport Processing/day4.js')
day4.run(fs.readFileSync('./Day4 - Passport Processing/input.txt'));

// Day 5: Binary Boarding
const day5 = require('./Day5 - Binary Boarding/day5.js')
day5.run(fs.readFileSync('./Day5 - Binary Boarding/input.txt'));

// Day 6: Custom Customs
const day6 = require('./Day6 - Custom Customs/day6.js')
day6.run(fs.readFileSync('./Day6 - Custom Customs/input.txt'));

// Day 7: Handy Haversacks
const day7 = require('./Day7 - Handy Haversacks/day7.js')
day7.run(fs.readFileSync('./Day7 - Handy Haversacks/input.txt'));

// Day 8: Handheld Halting
const day8 = require('./Day8 - Handheld Halting/day8.js')
day8.run(fs.readFileSync('./Day8 - Handheld Halting/input.txt'));

// Day 9: Encoding Error
const day9 = require('./Day9 - Encoding Error/day9.js')
day9.run(fs.readFileSync('./Day9 - Encoding Error/input.txt'));

// Day 10: Adapter Array
const day10 = require('./Day10 - Adapter Array/day10.js')
day10.run(fs.readFileSync('./Day10 - Adapter Array/input.txt'));

// Day 11: Seating System
const day11 = require('./Day11 - Seating System/day11.js')
day11.run(fs.readFileSync('./Day11 - Seating System/input.txt'));

// Day 12: Rain Risk
const day12 = require('./Day12 - Rain Risk/day12.js')
day12.run(fs.readFileSync('./Day12 - Rain Risk/input.txt'));

// Day 13: Shuttle Search
const day13 = require('./Day13 - Shuttle Search/day13.js')
day13.run(fs.readFileSync('./Day13 - Shuttle Search/input.txt'));

// Day 14: Docking Data
const day14 = require('./Day14 - Docking Data/day14.js')
day14.run(fs.readFileSync('./Day14 - Docking Data/input.txt'));

// Day 15: Rambunctious Recitation
const day15 = require('./Day15 - Rambunctious Recitation/day15.js')
day15.run();