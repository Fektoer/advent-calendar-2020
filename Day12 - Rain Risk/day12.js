const p = require('perf_hooks').performance;

var directions = []
var currentDirection = 'E';
var waypoint = { x: 10, y: -1 } 
var position = { x: 0,  y: 0 }

main = (input) => {
    var startTime = p.now()
    console.log("-----------------------------------------------");
    parseInput(input);
    console.log("Day 12A: " + calculatePositionA() + " in " + (p.now() - startTime) + " ms")

    // Reset position for B
    position.x = 0;
    position.y = 0;
    console.log("Day 12B: " + calculatePositionB() + " in " + (p.now() - startTime) + " ms")
}

calculatePositionA = () => {
    for (let i = 0; i < directions.length; i++) {
        movementCommand = directions[i];
        switch (movementCommand[0]) {
            case 'F':
                move(currentDirection, movementCommand[1], position)
                break;
            case 'R': // fall through
            case 'L':
                rotateShip(movementCommand[0], movementCommand[1])
                break;
            default:
                move(movementCommand[0], movementCommand[1], position)
                break;
        }
    }
    return Math.abs(position.x) + Math.abs(position.y);
}

calculatePositionB = () => {
    for (let i = 0; i < directions.length; i++) {
        movementCommand = directions[i];
        switch (movementCommand[0]) {
            case 'F':
                moveShipToWayPoint(movementCommand[1]);
                break;
            case 'R': // fall through
            case 'L':
                rotateWaypoint(movementCommand[0], movementCommand[1])
                break;
            default:
                move(movementCommand[0], movementCommand[1], waypoint)
                break;
        }
    }
    return Math.abs(position.x) + Math.abs(position.y);
}

moveShipToWayPoint = (value) => {
    position.x += waypoint.x * value;
    position.y += waypoint.y * value;
}

rotateWaypoint = (direction, value) => {
    var rotate = () => {
        var x = waypoint.x;
        var y = waypoint.y;
        switch (direction) {
            case 'L':
                waypoint.x = y;
                waypoint.y = x * -1;
                break;
            case 'R':
                waypoint.x = y * -1;
                waypoint.y = x;
                break;
        }
    }

    var ticks = value / 90;
    for (let i=0; i<ticks; i++){
        rotate();
    }
}

rotateShip = (direction, value) => {
    var compass = ['N', 'E', 'S', 'W'];             

    rotate = () => {
        var index = compass.indexOf(currentDirection);
        switch (direction){
            case 'L':
                index -= 1;
                if (index < 0){
                    index = compass.length-1;
                }
                break;
            case 'R':
                index += 1;
                if (index > compass.length-1){
                    index = 0;
                }
                break;
        }
        currentDirection = compass[index];
    }

    var ticks = value / 90;
    for (let i=0; i<ticks; i++){
        rotate();
    }
}

move = (direction, value, point) => {
    switch (direction) {
        case 'N':
            point.y -= value;
            break;
        case 'E':
            point.x += value;
            break;
        case 'S':
            point.y += value;
            break;
        case 'W':
            point.x -= value;
            break;
    }
}

parseInput = (raw) => {
    raw.toString()
        .replace(/\r/g, "")
        .split(/\n/)
        .map(function (x) {
            directions.push([x.slice(0, 1), parseInt(x.slice(1))])
        })
}

module.exports = {
    run: main
}
