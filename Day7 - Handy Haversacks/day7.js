const p = require('perf_hooks').performance;

var bagDictionary = [];

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    prepareInput(input);
    console.log("Day 7A: " + countBagsContainingShinyGoldBag() + " in " + (p.now() - startTime) + " ms");
    console.log("Day 7B: " + countBagsInShinyGoldBag() + " in " + (p.now() - startTime) + " ms");
}

prepareInput = (raw) => {
    splittedString = raw.toString()
        .replace(/\r/g, "")
        .split(/\n/);

    for (i = 0; i < splittedString.length - 1; i++) {
        bagContains = [];
        let bagRules = splittedString[i].split(/ bags(.+)/);
        let appearance = bagRules[0];
        let rules = bagRules[1].slice(9);

        rules.split(/, /)
            .map(appearance => appearance.replace(/\./, ""))
            .forEach(characteristic => {
                if (characteristic !== 'no other bags') {
                    characteristics = characteristic.split(/(?<=\d)(?=\D)/);
                    bagContains.push({
                        count: characteristics[0],
                        bag: new Bag(characteristics[1].slice(1).replace(/ bags| bag/g, ""))
                    })
                }
            });
        bagDictionary[appearance] = new Bag(appearance, bagContains);
    }
}

countBagsInShinyGoldBag = () => {
    return bagDictionary['shiny gold'].countTotalBags();
}

countBagsContainingShinyGoldBag = () => {
    var count = 0;
    for (let appearance in bagDictionary) {
        if (bagDictionary[appearance].hasShinyGoldBag()) {
            count++
        }
    }
    return count;
}

class Bag {
    constructor(appearance, contains) {
        this.appearance = appearance;
        this.contains = contains;
    }

    getAppearance = () => {
        return this.appearance;
    }

    countTotalBags = () => {
        let total = 0;
        for (let i = 0; i < this.contains.length; i++) {
            var numberOfBags = parseInt(this.contains[i].count);
            var totalBags = numberOfBags + (numberOfBags * bagDictionary[this.contains[i].bag.getAppearance()].countTotalBags());
            total += totalBags;
        }
        return total;

    }

    hasShinyGoldBag = () => {
        var found = false;
        for (let i = 0; i < this.contains.length; i++) {
            if (!found) {
                if (this.contains[i].bag.getAppearance() === 'shiny gold') {
                    found = true;
                } else {
                    found = bagDictionary[this.contains[i].bag.getAppearance()].hasShinyGoldBag()
                }
            }
        }
        return found;
    }
}

module.exports = {
    run: main
}
