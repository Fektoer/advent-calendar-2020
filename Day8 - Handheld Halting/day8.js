const p = require('perf_hooks').performance;

var operations = [];
var fullyExecuted = false;

main = (input) => {
    var startTime = p.now()
    console.log("-----------------------------------------------");
    parseInput(input);
    console.log("Day 8A: " + executeOperations(operations)  + " in " + (p.now() - startTime) + " ms");
    console.log("Day 8B: " + executeB() + " in " + (p.now() - startTime) + " ms");
}

executeB = () => {
    for (var i = 0; i < operations.length; i++) {
        operation = operations[i];
        if (operation.command.match(/nop|jmp/)) {

            // Copy the operations into a new array and modify the command
            var modifiedOperations = operations.map((x) => x);
            var newOperation = {
                command: (operation.command === 'nop') ? 'jmp' : 'nop',
                number: operation.number
            }
            modifiedOperations[i] = newOperation;

            // Execute the modified set of operations and check if you reached the end
            var accumulator = executeOperations(modifiedOperations);
            if (fullyExecuted) {
                return accumulator;
            }
        }
    }
}

executeOperations = (array) => {
    var executedOperations = new Map();
    var accumulator = 0;
    var index = 0;

    // While operation has not yet been executed
    while (!executedOperations.has(index)) {

        // Reached the end, return accumulator
        if (index === array.length) {
            fullyExecuted = true;
            return accumulator;
        }

        // Execute command
        var operation = array[index];
        executedOperations.set(index, operation);
        switch (operation.command) {
            case 'acc':
                accumulator += parseInt(operation.number);
                index++;
                break;
            case 'jmp':
                index += parseInt(operation.number);
                break;
            case 'nop':
                index++;
                break;
        }
    }

    // Encountered an operation we already executed
    return accumulator;
}

parseInput = (raw) => {
    raw.toString()
        .replace(/\r/g, "")
        .split(/\n/)
        .map(line => {
            operations.push({
                command: line.substring(0, 3),
                number: line.slice(4)
            })
        })
}

module.exports = {
    run: main
}
