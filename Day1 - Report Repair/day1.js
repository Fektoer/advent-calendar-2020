const p = require('perf_hooks').performance;

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    table = input.toString().replace(/\r/g, "").split("\n").map(value => parseInt(value));
    console.log("Day 1A: " + findSumDouble(table) + " in " + (p.now() - startTime) + " ms");
    console.log("Day 1B: " + findSumTriple(table) + " in " + (p.now() - startTime) + " ms");
}

findSumDouble = (table) => {
    for (i = 0; i < table.length; i++) {
        if (table.includes(2020 - table[i])) {
            return table[i] * table[table.indexOf(2020 - table[i])];
        }
    }
}

findSumTriple = (table) => {
    for (i = 0; i < table.length; i++) {
        for (j = 0; j < table.length; j++) {
            if (table.includes(2020 - table[i] - table[j])) {
                return table[i] * table[j] * table[table.indexOf(2020 - table[i] - table[j])];
            }
        }
    }
}

module.exports = {
    run: main
}