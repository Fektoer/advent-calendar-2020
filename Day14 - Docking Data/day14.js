const p = require('perf_hooks').performance;

var memory = [];
var commandSet = [];

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    parseInput(input);
    console.log("Day 14A: " + calculate(runSetA) + " in " + (p.now() - startTime) + " ms");

    memory = [];
    console.log("Day 14B: " + calculate(runSetB) + " in " + (p.now() - startTime) + " ms");
}

calculate = (setFunction) => {
    for (let i = 0; i < commandSet.length; i++) {
        setFunction(commandSet[i]);
    }

    return Object.keys(memory).reduce(function (sum, key) {
        return sum + memory[key];
    }, 0);
}

runSetA = (set) => {
    var mask = set[0].substr(7);
    for (let i = 1; i < set.length; i++) {
        var address = set[i].match(/\[(.*)\]/).pop();
        var value = parseInt(set[i].split("= ").pop());
        memory[address] = parseInt(applyMaskA(mask, convertTo36BitString(value)), 2);
    }
}

runSetB = (set) => {
    var mask = set[0].substr(7);
    for (let i = 1; i < set.length; i++) {
        var address = parseInt(set[i].match(/\[(.*)\]/).pop());
        var value = parseInt(set[i].split("= ").pop());
        var maskedAddress = applyMaskB(mask, convertTo36BitString(address));
        var addressVariants = generateAddressVariants(convertTo36BitString(maskedAddress));
        for (let i = 0; i < addressVariants.length; i++) {
            memory[addressVariants[i]] = value;
        }
    }
}

applyMaskA = (mask, bitString) => {
    var chars = bitString.split('');
    for (let i = 0; i < mask.length; i++) {
        if ((mask.charAt(i) !== 'X') && (mask.charAt(i) !== chars[i])) {
            chars[i] = mask.charAt(i);
        }
    }
    return chars.join('');
}

applyMaskB = (mask, bitString) => {
    var chars = bitString.split('');
    for (let i = 0; i < mask.length; i++) {
        if (mask.charAt(i) === 'X' || mask.charAt(i) === '1') {
            chars[i] = mask.charAt(i);
        }
    }
    return chars.join('');
}

generateAddressVariants = (bitString) => {
    var addressArray = [];
    var chars = bitString.split('');
    var count = (bitString.match(/X/g)).length;
    var variants = [];

    // if the mask contains n floating bits, the amount of variants are 2^n
    // So we construct a bit strings for each variant. Example, if the mask contains
    // 3 floating bits, the variants are 000, 001, 010, 011, etc) 
    for (let i = 0; i < Math.pow(2, count); i++) {
        variants.push(i.toString(2).padStart(count, "0"));
    }

    // For each variant we construct the new address by concatenating characters of the address.
    // Whenever we come across a floating bit, we replace it with the corresponding character in the 
    // variant. For a mask with 3 floating bits it results in 2^3 unique addresses.
    for (let j = 0; j < variants.length; j++) {
        let index = 0;
        let address = '';
        chars.forEach(char => {
            if (char === 'X') {
                address += variants[j][index];
                index++;
            } else {
                address += char;
            }
        })
        addressArray.push(parseInt(address, 2));
    }
    return addressArray;
}

convertTo36BitString = (number) => {
    return number.toString(2).padStart(36, "0");
}

parseInput = (raw) => {

    // Break it down in sets of 1 mask, N values
    var set = [];
    raw.toString()
        .replace(/\r/g, "")
        .split(/\n/)
        .map(function (line) {
            if (line.substr(0, 4) === 'mask') {
                if (set.length != 0) {
                    commandSet.push(set);
                    set = []
                }
            }
            set.push(line);
        })
    commandSet.push(set);
}

module.exports = {
    run: main
}