const p = require('perf_hooks').performance;

var rows = [];
var columns = [];

main = (input) => {
    var startTime = p.now();
    console.log("-----------------------------------------------");
    let arrayOfSeatIds = createArrayOfSeatIds(parseInput(input));
    console.log('Day 5A: ' + getMax(arrayOfSeatIds) + " in " + (p.now() - startTime) + " ms");
    console.log('Day 5B: ' + getMissingSeatId(arrayOfSeatIds) + " in " + (p.now() - startTime) + " ms");
}


parseInput = (raw) => {
    return raw.toString().replace(/\r/g, "").split("\n")
}

getMissingSeatId = (table) => {
    return table.sort().filter(seat => !table.includes(seat+1) && table.includes(seat+2))[0]+1;
}

createArrayOfSeatIds = (table) => {
    return theEasyWay(table);
    //return theHardWay(table);
    
}

theEasyWay = (table) => {
    var arrayOfSeatIds = [];
    for (i = 0; i < table.length; i++) {
        code = table[i];
        if (code !== '') {
            byte = code.replace(/B|R/g, 1).replace(/F|L/g, 0);
            rowId = parseInt(byte.substr(0, 7), 2);
            seatId = parseInt(byte.substr(7, 3), 2);
            arrayOfSeatIds.push(rowId * 8 + seatId);
        }
    }
    return arrayOfSeatIds;
}

theHardWay = (table) => {
    prepTables();
    var arrayOfSeatIds = [];
    for (i = 0; i < table.length; i++) {
        arrayOfSeatIds.push(createSeatId(table[i]));
    }
    return arrayOfSeatIds;
}

prepTables = () => {
    for (i = 0; i < 128; i++) { rows.push(i); }
    for (i = 0; i < 8; i++) { columns.push(i); }
}

createSeatId = (code) => {
    var rowCode = code.substr(0, 7);
    var columnCode = code.substr(7, 3);

    rowId = getId(rows, rowCode);
    columnId = getId(columns, columnCode)
    return rowId * 8 + columnId;
}

getId = (array, code) => {
    slicedArray = [...array];
    for (j = 0; j < code.length; j++) {
        slicedArray = sliceArrayByCode(slicedArray, code.charAt(j));
    }
    return slicedArray[0];
}

sliceArrayByCode = (array, code) => {
    if (code === 'B' || code === 'R') {
        return array.slice((array.length / 2), array.length);
    } else {
        return array.slice(0, (array.length / 2))
    }
}

getMax = (array) => {
    return array.reduce(function (a, b) {
        return Math.max(a, b);
    })
}

module.exports = {
    run: main
}
